#!/bin/bash

terraform apply
echo "[all]" > ../ansible/hosts ; terraform output platform_details >> ../ansible/hosts
cd ../ansible
ansible-playbook nginx_php.yml
cd ../terraform
terraform output elb_details | cut -d' ' -f 2 | sed 's/^/www./'
terraform output elb_details | cut -d' ' -f 2 | sed 's/^/www./;  s:$:/info.php:'
