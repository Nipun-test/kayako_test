// provider setup
provider "aws" {
  region                  = "${var.aws_region}"
  profile                 = "${var.aws_profile}"
  shared_credentials_file = "${var.aws_credentials_file}"
}

// query for ubuntu ami
data "aws_ami" "base_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/ebs/ubuntu-trusty-14.04-amd64-*"]
  }
}

// key pair
resource "aws_key_pair" "ec2_key" {
  key_name   = "${var.ssh_public_key_name}"
  public_key = "${var.ssh_public_key}"
}

// security groups
resource "aws_security_group" "elb-sg" {
  name        = "${var.firstname}-elb-sg"
  description = "${var.firstname}-elb-sg security rule"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// security groups
resource "aws_security_group" "ec2-sg" {
  name        = "${var.firstname}-ec2-sg"
  description = "${var.firstname}-ec2-sg security rule"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.elb-sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// ec2
resource "aws_instance" "ec2" {
  ami                    = "${data.aws_ami.base_image.id}"
  instance_type          = "${var.server_size}"
  count                  = "${var.ec2_instance_count}"
  key_name               = "${aws_key_pair.ec2_key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.ec2-sg.id}"]
  monitoring             = true
  tags {
    Name = "${var.firstname}-ec2-${count.index}"
  }
}

// Create a new load balancer
resource "aws_elb" "nginx" {
  name               = "nginx-elb"
  availability_zones = ["us-east-1b", "us-east-1c", "us-east-1d"]

  access_logs {
    bucket   = "nginx29"
    interval = 60
    enabled  = false
  }

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 10
  }

  instances                   = ["${aws_instance.ec2.*.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups             = ["${aws_security_group.elb-sg.id}"]

  tags {
    Name = "nginx-elb"
  }
}


output "platform_details" {
  value = ["${aws_instance.ec2.0.public_ip}"]
}

output "elb_details" {
  value = ["${aws_elb.nginx.id}, ${aws_elb.nginx.dns_name}"]
}

output "elb-sg-details" {
  value = ["${aws_security_group.elb-sg.vpc_id}, ${aws_security_group.elb-sg.id}"]
}
